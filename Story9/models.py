from django.db import models
from datetime import datetime, date

# Create your models here.
class Data(models.Model):
    tgl = models.DateTimeField()
    cat = models.CharField(max_length=30)

    def as_dict(self):
        return {
            "tgl": self.tgl,
            "cat": self.cat,
        }
